import React from "react";
import "../../Assets/Css/Body.scss";
import Search from "./Search";
import Navbar from "./Navbar";
import GroupList from "./GroupList";
import groupList from "../Services/GroupListService";
import { connect } from "react-redux";

class Body extends React.Component {
  state = {};
  render() {
    return (
      <section className="Body">
        <Navbar />
        <h1 className="TitleBoby">
          سفارش آنلاین غذا از بهترین رستوران‌های اطراف شما تهران
        </h1>

        <Search />
        <div className="GroupList">
          {groupList.map(function (item, index) {
            return (
              <GroupList dataproducts={item} src={item} title={item.name} />
            );
          })}
        </div>
      </section>
    );
  }
}

export default connect()(Body);
