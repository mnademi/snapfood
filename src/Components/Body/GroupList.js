import React, { Component } from "react";
import pic from "../../Assets/Img/restaurant.png";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
class GroupList extends Component {
  render() {
    return (
      <figure>
        <Link to={`/Catalog/${this.props.dataproducts.id}`}>
          <img
            src={this.props.dataproducts.src}
            title={this.props.dataproducts.title}
          />
        </Link>
      </figure>
    );
  }
}

export default connect()(GroupList);
