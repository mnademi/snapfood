import React from "react";
import imagelogo from "../../Assets/Img/snappfood_logo.svg";
import "../../Assets/Css/Body.scss";

class Navbar extends React.Component {
  state = {};
  render() {
    return (
      <section>
        <img className="logo" src={imagelogo} />
        <div className="Nav_box">
          <div className="userIcon"></div>
          <div>ورود/عضویت</div>
        </div>
      </section>
    );
  }
}

export default Navbar;
