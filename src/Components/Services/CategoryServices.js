let CategoryServices = [
    {
        
        GroupID : 1,
        id : 1,
        name : "قنادی شیرین سرا",
        address: 'شاهرود، خیابان فردوسی، نزدیک میدان امام، جنب خدماتی عجم',
        logo : '/Img/confectionary2.jpg',
        image:'/Img/confectionary2.jpg',
        Address:'یوسف آباد،  جهان آرا،  مطهری، ...'
     },
     {
        GroupID : 1,
        id : 2,
        name : "قنادی بهار",
        address: 'میامی - نردین خیابان امام حسین جنب مسجد جامع',
        logo :  '/Img/confectionary2.jpg'
        ,
        Address:'یوسف آباد،  جهان آرا،  مطهری، ...'
     },
     {
        GroupID : 1,
        id : 3,
        name : "شيريني سراي كام",
        address: 'بسطام ميدان مركزي',
        logo : '/Img/qanadi1.jpg',
        Address:'یوسف آباد،  جهان آرا،  مطهری، ...'
     },
     {
        
        GroupID : 2,
        id : 4,
        name : "قهوه چینو",
        address: 'خیابان شهید مدنی روبروی خیابان 15 خرداد',
        logo :  '/Img/cofe1.jpg',
        Address:'یوسف آباد،  جهان آرا،  مطهری، ...'
     },
     {
        GroupID : 2,
        id : 5,
        name : "كافه تريا",
        address: 'خيابان شهيد صدوقي نرسيده به فضاي سبز',
        logo :  '/Img/cofe2.jpg',
        Address:'یوسف آباد،  جهان آرا،  مطهری، ...'
     },
     {
        GroupID : 2,
        id : 6,
        name : "بستني شاه بيكيان",
        address: '	خيابان فردوسي پايين تر از پارك',
        logo : '/Img/cofe3.jpg',
        Address:'یوسف آباد،  جهان آرا،  مطهری، ...'
     },
     {
        
        GroupID : 3,
        id :7,
        name : "رستوران بین‌المللی نارنجستان",
        address: 'تهران، سعادت‌آباد، بین میدان سرو و کاج، میدان شهرداری، مجتمع سرو',
        logo :  '/Img/confectionary2.jpg',
        Address:'یوسف آباد،  جهان آرا،  مطهری، ...'
     },
     {
        GroupID : 3,
        id : 8,
        name : "رستوران سنتی شهربانو شهرک غرب",
        address: 'شهرک غرب، بلوار دادمان، بلوار درختی، مجتمع تجاری خلیج فارس',
        logo :  '/Img/confectionary2.jpg',
        Address:'یوسف آباد،  جهان آرا،  مطهری، ...'
     },
     {
        GroupID : 3,
        id : 9,
        name : "رستوران سنتی سروستان",
        address: 'تهران، همت غرب، شهید خرازی،خروجی دریاچه شهدای چیتگر',
        logo :  '/Img/confectionary2.jpg',
        Address:'یوسف آباد،  جهان آرا،  مطهری، ...'
     },
    {
        
        GroupID : 4,
        id : 10,
        name : "شهروند",
        address: 'بزرگراه آفریقا، خیابان شهید سعیدی (حافظ شیرازی) شماره  ۴',
        logo :  '/Img/confectionary2.jpg',
        Address:'یوسف آباد،  جهان آرا،  مطهری، ...'
     },
     {
        GroupID : 4,
        id : 11,
        name : "کیوان",
        address: 'تهران،خیابان شیخ بهایی جنوبی، خیابان اتحاد، پلاک ۲۱',
        logo :  '/Img/confectionary2.jpg',
        Address:'یوسف آباد،  جهان آرا،  مطهری، ...'
     },
     {
        GroupID : 4,
        id : 12,
        name : " اتکا ",
        address: ' خ امام خمینی بعد از میدان حسن آباد خ باستیون غربی خ شهید مرادی ساختمان اتکا',
        logo :  '/Img/confectionary2.jpg',
        Address:'یوسف آباد،  جهان آرا،  مطهری، ...'
     }
]
export default  CategoryServices;