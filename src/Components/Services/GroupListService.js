
let groupList = [
  {
    id: 1,
    name: "شیرینی",
    src: "/Img/confectionery.png",
    quantity: 5
  },
  { id: 2, name: "کافه", src: "/Img/cafe.png", quantity: 5 },
  {
    id: 3,
    name: "رستوران",
    src: "/Img/restaurant.png",
    quantity: 5
  },
  {
    id: 4,
    name: "سوپر مارکت",
    src: "/Img/supermarket.png",
    quantity: 5
  }
];
export default groupList;
