import { createStore } from "redux";
import ShopReducer from "../ShopReducer/ShopReducer";

export default (ShopDefault) => {
  const store = createStore(ShopReducer, ShopDefault);
  return store;
};
