import React, { Component } from "react";

import CategoryServices from "../../Services/CategoryServices";
import Header from "../Header";
import Search from "../../Body/Search";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
let datas = CategoryServices;

class Catalog extends Component {
  render() {
    var p = datas.filter(d => d.GroupID == this.props.match.params.id);
    console.log('datas',this.props.match.params.id)
    return (
  
      <section>
        <Header />
        <Search />
        <div className="d-flex">
          <div className="col-md-9">
            <div className="row">
              {p.map((item, index) => {
                return (
                  <div key={index} className="col-md-4 ">
                    <div className="catalog-content">
                      <div
                        className="img-catalog"
                        style={{ backgroundImage: "URL(" + item.logo + ")" }}
                      >
                        <div className="boxinside">پیش سفارش</div>
                      </div>
                      <div className="catalog-title">
                        <Link to>{item.name} </Link>
                      </div>
                      <div className="catalog-Address">{item.Address}</div>
                      <div className="btn-catalog-send">
                      <Link to={`/Products/${item.id}`}className="btn btn-outline-danger"> پیش سفارش</Link>
                      </div>
                     
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
          <div className="col-md-3"></div>
        </div>
      </section>
    );
  }
}

export default connect()(Catalog);
