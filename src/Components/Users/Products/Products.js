import React, { Component } from "react";
import ProductsServices from "../../Services/ProductsService";
import Header from "../Header";
import ShopingCart from "../ShopingCart/ShopingCart";
import {ShpIncrement,ShpDecrement} from '../../Action'
import { connect } from "react-redux";
let prs = ProductsServices;
var ara = prs.filter((k) => k.ID == 0);
var ids = 0;
var cart = [];
const Products =({dispatch,conterShop})=> {

  var product=prs.filter(k=>k.CatalogID==window.location.pathname.substring(10,11))
    return (
      <section className="productsAlign">
        <Header />
        <div className="d-flex">
          <div className="col-md-9">
            <div className="row">
              {product.map((item, index) => {
                return (
                  <div className="col-md-3">
                    <div className="product-total">
                      <div
                        className="img-product"
                        style={{ backgroundImage: "URL(" + item.image + ")" }}
                      ></div>

                      <h4 className="title-price">{item.name}</h4>
                      <div className="product-price">
                        {item.Price}

                        <span
                          className="plusIcon"
                          onClick={()=>dispatch(ShpIncrement(item))}
                        >
                          +
                        </span>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
          <div className="col-md-3">
            <div className="title-cart">
              <h6>سبد خرید</h6>
            </div>
            {/* {this.state.cartarray.length > 0 && (
              <ShopingCart
                AddToCart={this.AddToCart}
                Subquantity={this.Subquantity}
                cartarray={this.state.cartarray}
              />
            )} */}
            <ShopingCart conterShop={conterShop} />
          </div>
        </div>
      </section>
    );
  }


export default connect(state=>{
  return {conterShop:state}
})(Products) ;
