import React, { Component, createContext } from "react";
import ProductsServices from "../../Services/ProductsService";
import {ShpIncrement,ShpDecrement} from '../../Action'
import { connect } from "react-redux";

//var TotalPrice = null;
const ShopingCart=({dispatch,total,TotalPrice})=>{
    return (
      <div className="">
       
        
        {total.map((item, index) => {
          TotalPrice += item.Price * item.quantity;
          return (
            <div key={index} className="shoping-cart">
              <p>{item.name}</p>
              <p>
                {item.price}
                تومان&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a
                  onClick={() =>dispatch(ShpIncrement(item))}
                  className="plusIcon al"
                >
                  +
                </a>
                <span className="plusIcon al">{item.quantity}</span>
                <a
                  onClick={() => dispatch(ShpDecrement(item))}
                  className="plusIcon al"
                >
                  -
                </a>
              </p>
            </div>
          );
        })}
        <p>جمع کل</p>
        {TotalPrice}
      </div>
    );
  }


export default connect(state=>{

  return {total:state.filter(t=>t.quantity>0),TotalPrice:null}
})(ShopingCart) ;
