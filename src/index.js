import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { BrowserRouter, Route } from "react-router-dom";
import * as serviceWorker from "./serviceWorker";
import Catalog from "./Components/Users/Products/Catalog";
import Products from "./Components/Users/Products/Products";
import configureStore from "./Components/ConfigureStore/ConfigureStore";
import ProductsServices from "./Components/Services/ProductsService";
import { Provider } from "react-redux";
const store = configureStore(ProductsServices);
const routing = (
  <Provider store={store}>
    <BrowserRouter>
    <div>
      <Route path="/Catalog/:id" component={Catalog} />
      <Route path="/Products/:id" component={Products} />
      <Route exact path="/" component={App} />
    </div>
  </BrowserRouter>
  </Provider>
  
);
ReactDOM.render(routing, document.getElementById("root"));
serviceWorker.unregister();
